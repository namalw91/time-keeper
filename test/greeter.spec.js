"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var greeter_1 = require("../src/greeter");
var chai = require("chai");
var expect = chai.expect;
describe("greeter", function () {
    it("should greet with message", function () {
        var greeter = new greeter_1.Greeter("friend");
        expect(greeter.greet()).to.equal("Bonjour, friend!");
    });
});
