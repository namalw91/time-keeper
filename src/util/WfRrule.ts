export interface WfRrule<T>{
  getExecutionDate(options:T):Date;
  // calculateRecurrentDate(executionCount:number,options:T);
  // calculateRecurrentDate(options:object);
  setNextExecutionConfig(data):object;
  generateRuleOptions(config:any):T;
}
