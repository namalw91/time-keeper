
import { RRule } from "rrule";
import {WfRrule} from "../WfRrule";
//todo : use adaptor pattern
export class WfRruleImpl implements WfRrule<RRule.Options>{

  generateRuleOptions(config: any): RRule.Options {
    //todo : improve this
    let result = this.formatConfig(config);
    return <RRule.Options>result;
  }

  // calculateRecurrentDate(executionCount,options: RRule.Options) {
  //   if(!options.freq){
  //     throw Error("Mandatory field \"freq\" is not set in getNextExecution()")
  //   }
  //
  //   options.interval =  (executionCount +1);
  //   return options;
  // }

  getExecutionDate(options: RRule.Options) {
    if(!options.freq){
      throw Error("Mandatory field \"freq\" is not set in getNextExecution()")
    }
    let rule = new RRule(options)
    return rule.all()[1];
  }

  /*
  * This is to avoid processing large number of dates
  * */
  setNextExecutionConfig(data):object{
    data.wfScheduleData.numberOfExecution =  (data.wfScheduleData.numberOfExecution +1);
    data.wfScheduleData.rRule.interval =  (data.wfScheduleData.numberOfExecution*data.wfScheduleData.startInterval);
    return data;
  }

  private getRruleFrequency(recurrenceType):RRule.Frequency{
    let frequency  = {
      hours : RRule.YEARLY,
      daily : RRule.DAILY,
      weekly : RRule.WEEKLY,
      monthly : RRule.MONTHLY,
      yearly : RRule.YEARLY,
    };
    return frequency[recurrenceType];
  }

  private getRruleDayOfTheWeek(dayOfTheWeek): object{
    let days = {
        MON:RRule.MO,
        TUE:RRule.TU,
        WED:RRule.WE,
        THU:RRule.TH,
        FRI:RRule.FR,
        SAT:RRule.SA,
        SUN:RRule.SU
    }

    return days[dayOfTheWeek]
  }

  private formatConfig(config) {
    let formatConfig = {
      freq: "",
      dtstart: "",
      interval: "",
      wkst: "",
      count: "",
      until: "",
      bysetpos: "",
      bymonth: "",
      bymonthday: "",
      byyearday: "",
      byweekno: "",
      byweekday: "",
      byhour: "",
      byminute: "",
      bysecond: "",
      byeaster: ""
    }

    return {
      freq: RRule.MINUTELY,
      dtstart: new Date(),
      count: 2,
      interval: 1,
    }
  }
}
