import * as express from "express";
import * as logger from "morgan";
import * as cookieParser from "cookie-parser";
import * as bodyParser from "body-parser";
import * as compress from "compression";
import * as path from "path";
import * as methodOverride from "method-override";
import * as glob from "glob";
import * as Agenda from "agenda";

const rootPath = path.normalize(__dirname + "/..");

export class ExpressConfig {
  private app: any;

  constructor(app: express.Express) {
    this.app = app;
    this.createExpress();
  }

  private createExpress() {
    const env: string = process.env.NODE_ENV || "development";
    this.app.locals.ENV = env;
    this.app.locals.ENV_DEVELOPMENT = env === "development";
    const self = this;

    this.app.use((req: any, res: any, next: any) => {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Methods", "POST,GET,OPTIONS,PUT,DELETE");
      res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,authorization, access_token");
      next();
    })
      .options("*", (req: any, res: any, next: any) => {
        res.end();
      });

    this.app.use(logger("dev"));
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({
      extended: true
    }));
    this.app.use(cookieParser());
    this.app.use(compress());
    this.app.use(methodOverride());

    const path: string = rootPath + "/routers/*.js";
    console.log("express config ->" + path);
    const routers = glob.sync(path);
    routers.forEach((controller) => {
      require(controller)(self.app);
    });

    this.app.use((req: any, res: any, next: any) => {
      const err: any = new Error("Not Found");
      err["status"] = 404;
      next(err);
    });

    if (this.app.get("env") === "development") {
      this.app.use((err: any, req: any, res: any) => {
        res.status(err["status"] || 500)
          .send({
            "message": err.message,
            "error": err,
            "title": "error"
          });
      });
    }

    // production error handler
    // no stack traces leaked to user
    this.app.use((err: any, req: any, res: any) => {
      res.status(err["status"] || 500)
        .send({
          "message": err.message,
          "error": {},
          "title": "error"
        });
    });



    // var Agenda = require('agenda');
    // var Agendash = require('agendash');
    //
    // var agenda = new Agenda({db: {address: 'mongodb://127.0.0.1/agenda'}});
// or provide your own mongo client:
// var agenda = new Agenda({mongo: myMongoClient})

    // this.app.use('/dash', Agendash(agenda));

  }

}
