import * as express from "express";
import * as Config from "config";
import * as Agenda from "agenda";
import { RRule, RRuleSet } from "rrule";
import {ServiceFactory} from "../services/factory/ServiceFactory";
import {JobService} from "../services/JobService";
import {ConnectionFactory} from "../connections/factory/ConnectionFactory";
import {WfRruleImpl} from "../util/impl/WfRruleImpl";
import {WfRrule} from "../util/WfRrule";
import * as winston from "winston";

// https://abdelhady.net/2015/02/06/solved-restarting-node-server-may-stop-any-recurring-agenda-jobs/
// https://github.com/jakubroztocil/rrule/issues/148

//issue
// https://github.com/agenda/agenda/issues/410


const uuid = require("node-uuid");
const router = express.Router();
const serviceFactory = new ServiceFactory();

let configExecuteOnce = {
  "scheduleId": uuid.v1,
  "scheduleName": "schedule",
  "startDate": "2017-04-20",
  "startTime": "8:30",
  "timeSuffix": "am",
  "timeZone": "Asia/Colombo",
  "recurrencePlan": "none",
  "range": {
    "isNoEndDate": false,
    "occurrences": null,
    "endDate": null
  },
  "status": {
    "active": "true",
    "paused": "false"
  },
  "data": {
    "workflowId": "id1",
    "initiatingActor": {
      "modifiedAt": "",
      "createdAt": "",
      "text": "",
      "email": "chamath.ariyawansa@workflows.com",
      "lastName": "Ariyawansa",
      "firstName": "Chamath",
      "_id": ""
    },
    "predefinedActors": [
      {
        "actors": [
          {
            "actorId": "58ca0cccf24a0b00126e8b0b",
            "createdAt": "2017-03-16T03:55:56.857Z",
            "email": "pubodanee.ranathunga@workflowsapp.com",
            "firstName": "Pubodanee",
            "lastName": "Ranathunga",
            "modifiedAt": "2017-03-16T03:55:56.857Z",
            "text": ""
          }
        ],
        "actor": {
          "actorId": "58ca0cccf24a0b00126e8b0b",
          "createdAt": "2017-03-16T03:55:56.857Z",
          "email": "pubodanee.ranathunga@workflowsapp.com",
          "firstName": "Pubodanee",
          "lastName": "Ranathunga",
          "modifiedAt": "2017-03-16T03:55:56.857Z",
          "text": ""
        },
        "role": {
          "id": "null",
          "isInternalActor": true,
          "name": "not"
        },
        "isStatic": true,
        "status": "met"
      }
    ],
    "teamId": "231",
    "teamName": "name"
  }
}


router.route(<string> Config.get("workflow.route.getAllTrigger"))
  .get((req, res) => {
    const jobService = <JobService> serviceFactory.getService("JobService");
    console.log("Get a trigger" + req.params.triggerId)
    let scheduleId = "123";
    jobService.getScheduleJobInfo(req.params.triggerId).then((data)=>{
      // status update success
      res.status(200)
        .send({
          'success': true,
          'data': {scheduleId : scheduleId,nextExecutionTrigger : data}
        });
    },(error)=>{
      //todo : check the error code
      winston.error(error.stack)
      res.status(500)
        .send({
          'success': false,
          'error': "Schedule disable failed "
        });
    });


    // var mongoConnectionString = Config.get("database.url").toString();
    //
    // const jobService1 = <JobService> serviceFactory.getService("JobService");
    // jobService1.scheduleJob(null,"Processos_http_call",{
    //   rrule:"rule_1"
    // });


 //////////////////////////////////////////////////////////////////////////
    // var mongoConnectionString = Config.get("database.url").toString();
    // var agenda = new Agenda({db: {address: mongoConnectionString}});
    // agenda.define('greet the world', function(job, done) {
    //   console.log('hello worldssssssssssssssssss!');
    //   done();
    // });
    //
    // agenda.on('ready', function() {
    //   // agenda.schedule('1 seconds', 'greet the world');
    //   agenda.every('1 seconds', 'greet the world',{testRRule : "testrRule"});
    //   agenda.start();
    // });
//////////////////////////////////////////////////////////////////////////

  });

router.route(<string> Config.get("workflow.route.createTrigger"))
  .post((req, res) => {
    console.log("Post a trigger")
    //todo: replace this with a UUID
    let scheduleId = "123";

    // var mongoConnectionString = Config.get("database.url").toString();

    let wfRruleImpl:WfRrule<RRule.Options> = new WfRruleImpl();
    let rule = wfRruleImpl.generateRuleOptions(configExecuteOnce);
    let date:Date = wfRruleImpl.getExecutionDate(rule);
    const jobService = <JobService> serviceFactory.getService("JobService");
    jobService.scheduleJob(date,"Processos_http_call",{
      wfScheduleData : {
        scheduleId:scheduleId,
        rRule: rule,
        numberOfExecution: 1,
        startInterval: rule.interval
      }
    }).then((success)=>{
      res.status(200)
        .send({
          'success': true,
          'data': {scheduleId : scheduleId,create : success}
        });

    },(fail)=>{
      res.status(500)
        .send({
          'success': false,
          'data': {scheduleId : scheduleId,create : fail}
        });
    });
  })

router.route(<string> Config.get("workflow.route.deleteTrigger"))
  .delete((req, res) => {

  let scheduleId = req.params.triggerId;
    const jobService = <JobService> serviceFactory.getService("JobService");
    jobService.deleteJob(scheduleId).then((success)=>{
      res.status(200)
        .send({
          'success': true,
          'data': {isDelete : success}
        });
    },(error)=>{
      //todo : check the error code
      winston.error(error.stack)
      res.status(500)
        .send({
          'success': false,
          'error': `Error occured deleteing schedule ${scheduleId}`
        });
    });
  });

// router.route(<string> Config.get("workflow.route.updateTrigger")).put((req,res)=>{
//   let wfRruleImpl:WfRrule<RRule.Options> = new WfRruleImpl();
//   let rule = wfRruleImpl.generateRuleOptions(configExecuteOnce);
//   let date:Date = wfRruleImpl.getExecutionDate(rule);
//   const jobService = <JobService> serviceFactory.getService("JobService");
//
//   jobService.updateSchedule(date,"Processos_http_call",{
//     wfScheduleData : {
//       scheduleId:"123",
//       rRule: rule,
//       numberOfExecution: 1,
//       startInterval: rule.interval
//     }
//   }).then((success)=>{
//     // status update success
//     res.status(200)
//       .send({
//         'success': true,
//         'data': {scheduleId : scheduleId,disable : success}
//       });
//   },(error)=>{
//     //todo : check the error code
//     winston.error(error.stack)
//     res.status(500)
//       .send({
//         'success': false,
//         'error': "Schedule disable failed "
//       });
//   });
//
// })


















//todo : implement create new schedule after deleting old one
router.route(<string> Config.get("workflow.route.pauseTrigger")).get((req,res)=>{
  const jobService = <JobService> serviceFactory.getService("JobService");
  console.log("pause a trigger")
  let scheduleId = req.params.triggerId;
  jobService.disableJob(scheduleId).then((success)=>{
    // status update success
    res.status(200)
      .send({
        'success': true,
        'data': {scheduleId : scheduleId,disable : success}
      });
  },(error)=>{
    //todo : check the error code
    winston.error(error.stack)
    res.status(500)
      .send({
        'success': false,
        'error': "Schedule disable failed "
      });
  });

})

router.route(<string> Config.get("workflow.route.activateTrigger")).get((req,res)=>{
  const jobService = <JobService> serviceFactory.getService("JobService");
  console.log("activate a trigger")
  let scheduleId = req.params.triggerId;
  jobService.enableJob(scheduleId).then((success)=>{
    // status update success
    res.status(200)
      .send({
        'success': true,
        'data': {scheduleId : scheduleId,enable : success}
      });
  },(error)=>{
    //todo : check the error code
    winston.error(error.stack)
    res.status(500)
      .send({
        'success': false,
        'error': "Schedule enable failed "
      });
  });
})

module.exports = (app: any) => {
  app.use(Config.get("workflow.route.base"), router);
};



