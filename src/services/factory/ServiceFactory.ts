import {JobDAOImpl} from "../../daos/impl/JobDAOImpl";
import {JobDAO} from "../../daos/JobDAO";
import {JobService} from "../JobService";
import {JobServiceImpl} from "../impl/JobServiceImpl";
import {HttpsUrlConnectionImpl} from "../../connections/impl/URLConnectionImpl";
import {URLConnection} from "../../connections/URLConnection";
import {ConnectionFactory} from "../../connections/factory/ConnectionFactory";

export class ServiceFactory{
  private service = null;
  private jobDAO:JobDAO;
  private jobService:JobService;

  constructor(){
    // jobDAO should be singleton
    this.jobDAO = new JobDAOImpl();
    this.jobService = new JobServiceImpl(this.jobDAO);

  }

  getService(serviceName : string){
    switch(serviceName){
      case 'JobService' : {
        this.service = this.jobService;
        break;
      }

      default :
        throw new Error("unsupported service type");
    }
    return this.service;
  }
}
