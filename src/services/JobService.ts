export interface JobService {
  scheduleJob(date: Date, name: string, data: any): Promise<boolean>;
  registerAllJob(): void;

  //todo : change object array into a "JobModel" model list
  getScheduleJobInfo(scheduleId: any): Promise<object[]>;
  deleteJob(scheduleId: any): Promise<boolean>;
  updateSchedule(date: Date, name: string, data: any):Promise<object>;
  disableJob(scheduleId:string): Promise<boolean>;
  enableJob(scheduleId:string): Promise<boolean> ;

  resumeAllPendingTriggers();
}
