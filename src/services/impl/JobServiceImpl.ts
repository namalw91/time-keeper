import {JobService} from "../JobService";
import {JobDAO} from "../../daos/JobDAO";
import {ConnectionFactory} from "../../connections/factory/ConnectionFactory";
import {DatabaseConnection} from "../../connections/DatabaseConnection";
import {URLConnection} from "../../connections/URLConnection";
import {MessageConnection} from "../../connections/MessageConnection";
import * as winston from "winston";
import {WfRrule} from "../../util/WfRrule";
import {WfRruleImpl} from "../../util/impl/WfRruleImpl";

export class JobServiceImpl implements JobService{
   private jobDAO: JobDAO;
   private wfRrule : WfRrule<RRule.Options>;
  constructor(jobDAO){
    this.jobDAO = jobDAO;
    this.wfRrule = new WfRruleImpl();
  }


  scheduleJob(date: Date, name: string, data: any): Promise<boolean> {
    return this.jobDAO.scheduleJob(date,name, data);
  }

  registerAllJob(): void {
    const connectionFactory = new ConnectionFactory();
    const httpConnection:URLConnection = <URLConnection> connectionFactory.getConnection("HttpsUrlConnection");
    const kafkaConnection = <MessageConnection> connectionFactory.getConnection("KafkaConnection");

    //Register both http and kafka jobs.
    this.jobDAO.registerJob("Processos_kafka_call", (data) => {
      kafkaConnection.connect();
      winston.info("topic: " + name + "message: " + JSON.stringify(data, null, 2) + "\n \n");
      kafkaConnection.send(name, data);
      return true;
    },(name, data)=>{
      let nextExecutionData = this.wfRrule.setNextExecutionConfig(data);
      let date  = this.wfRrule.getExecutionDate(data.wfScheduleData.rRule)
      this.scheduleJob(date, name, nextExecutionData);
    });

    this.jobDAO.registerJob("Processos_http_call", (data) => {
      httpConnection.connect();
      var postData = JSON.stringify({
        remoteCall: {execute : "/processos/init()"}
      });
      const options = {
        host: "httpbin.org",
        method: "POST",
        path: "/post",

        headers: {
          'Content-Type': 'application/json',
          'Content-Length': postData.length
        }
      };

      httpConnection.sendRequest(postData,options).then((success)=>{
        console.log("Response");
        console.log(JSON.parse(success).json);
      },(fail)=>{
        console.log(fail);
      })
      return true;
    },(name, data)=>{
      let nextExecutionData = this.wfRrule.setNextExecutionConfig(data);
      let date  = this.wfRrule.getExecutionDate(data.wfScheduleData.rRule)
      this.scheduleJob(date, name, nextExecutionData);
    });
  }

  getScheduleJobInfo(scheduleId: any): Promise<object[]>{
    return this.jobDAO.getScheduleJobInfo(scheduleId);
  }

  deleteJob(scheduleId: any): Promise<boolean> {
    return this.jobDAO.deleteJobs(scheduleId);
  }


  disableJob(scheduleId: string): Promise<boolean> {
    return this.jobDAO.disableJob(scheduleId);
  }

  enableJob(scheduleId: string): Promise<boolean> {
    return this.jobDAO.enableJob(scheduleId);
  }

  resumeAllPendingTriggers() {
    this.jobDAO.resumeAllPendingTriggers();
  }

  updateSchedule(date: Date, name: string, data: any): Promise<object> {
    return new Promise((resolve,reject)=>{
      this.deleteJob(data.scheduleId).then((success)=>{
        if(success){
          this.scheduleJob(date, name, data).then((success)=>{
            if(success){
              resolve({success:true,message:"Schedule updated"});
            }else {
              reject({success:false,message:"Create schedule unsuccessful"});
            }
          });
        }else{
          reject({success:false,message:"Delete schedule unsuccessful"});
        }
      });
    });
  }
}
