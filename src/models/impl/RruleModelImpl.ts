
import {Weekday} from "rrule";
import {RruleModel} from "../RruleModel";

export class RruleModelImpl implements RruleModel{
  freq: RRule.Frequency;
  dtstart?: Date;
  interval?: number;
  wkst?: number | Weekday;
  count?: number;
  until?: Date;
  bysetpos?: number | number[];
  bymonth?: number | number[];
  bymonthday?: number | number[];
  byyearday?: number | number[];
  byweekno?: number | number[];
  byweekday?: Weekday | Weekday[] | number | number[];
  byhour?: number | number[];
  byminute?: number | number[];
  bysecond?: number | number[];

  fill(source):void {
    this.freq = source.freq
    this.dtstart = source.dtstart
    this.interval = source.interval
    this.wkst = source.wkst
    this.count = source.count
    this.until = source.until
    this.bysetpos = source.bysetpos
    this.bymonth = source.bymonth
    this.bymonthday = source.bymonthday
    this.byyearday = source.byyearday
    this.byweekno = source.byweekno
    this.byweekday = source.byweekday
    this.byhour = source.byhour
    this.byminute = source.byminute
    this.bysecond = source.bysecond
  }

}
