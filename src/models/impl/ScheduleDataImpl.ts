import {ScheduleData} from "../ScheduleData";
import {RruleModel} from "../RruleModel";

export class ScheduleDataImpl implements ScheduleData{
  scheduleId: string;
  numberOfExecution: number;
  startInterval: number;
  rRule: RruleModel;

  fill(source: any):void {
    this.scheduleId = source.scheduleId;
    this.rRule = source.rRule;
    this.numberOfExecution = source.numberOfExecution;
    this.startInterval = source.startInterval;
  }

}
