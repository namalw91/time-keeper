import {Model} from "./Model";
import {RruleModel} from "./RruleModel";

export interface ScheduleData extends Model{
  scheduleId:string;
  rRule:RruleModel;
  numberOfExecution:number;
  startInterval:number

}
