import {JobDAO} from "../JobDAO";
import Agenda = require("agenda");
import {AgendaConnection} from "../../connections/AgendaConnection";
import {AgendaConnectionImpl} from "../../connections/impl/AgendaConnectionImpl";
import * as winston from "winston";
import {WfRrule} from "../../util/WfRrule";
import { RRule } from "rrule";
import {WfRruleImpl} from "../../util/impl/WfRruleImpl";
import {JobService} from "../../services/JobService";
import {ServiceFactory} from "../../services/factory/ServiceFactory";

export class JobDAOImpl implements JobDAO{

  private agendaConnection: AgendaConnection;

  private static agenda;

  options = {
    "type": "object",
    "properties": {
      "scheduleId": {
        "type": "string",
        "format": "uuid"
      },
      "scheduleName": {
        "type": "string"
      },
      "startDate": {
        "type": "string"
      },
      "startTime": {
        "type": "string"
      },
      "timeSuffix": {
        "type": "string"
      },
      "timeZone": {
        "type": "string"
      },
      "recurrencePlan": {
        "type": "object"
      },
      "status": {
        "type": "object",
        "properties": {
          "active": {
            "type": "boolean"
          },
          "paused": {
            "type": "boolean"
          }
        }
      },
      "range": {
        "type": "object",
        "properties": {
          "isNoEndDate": {
            "type": "boolean"
          },
          "occurrences": {
            "type": "number"
          },
          "endDate": {
            "type": "string"
          }
        }
      },
      "data": {
        "type": "object"
      }
    }
  }





  constructor(){
    this.agendaConnection = new AgendaConnectionImpl();

  }

  // todo : add graceful stop to index.ts
  agendaFailGracefully() {
    JobDAOImpl.agenda.stop(() => process.exit(0));
    console.log("Gracefully shutdown")
  }

  // Important: Done method should be called in order change job state from running to finish.
  // Not doing so will not re reccure.
  registerJob(name: string, taskTrigger: any,reScheduleCb: any): void {

    JobDAOImpl.agenda = this.agendaConnection.connect();
    //todo : move agendaFailGracefully to agendaConnection impl or index.ts
    process.on('SIGTERM', this.agendaFailGracefully);
    process.on('SIGINT', this.agendaFailGracefully);

    // JobDAOImpl.agenda.on("ready", () => {
      JobDAOImpl.agenda.define(name, (job: Agenda.Job, done: any) => {
        new Promise((resolve, reject) => {
          const data = job.attrs.data;
          const name = job.attrs.name;

          let isSuccess = taskTrigger(data);

          if(isSuccess){

            // let date = this.wfRrule.calculateRecurrentDate(data.wfScheduleData.numberOfExecution,<RRule.Options>data.rRule);
            reScheduleCb(name, data);
            resolve(true);
          }

        }).then((success)=>{
          console.log("Done...")
          done();
        })
          .catch((err) => {
            winston.error("JobDAOImpl registerJob:" + err.stack);
            done();
          });
      });
    // });
  }


  scheduleJob(date:Date, name: string, data: any): Promise<boolean> {
    //todo : handle errors in alternative scenarios.
    return new Promise((resolve,reject)=>{
      if (date == null) {
        JobDAOImpl.agenda.every('1 seconds' ,name, data);
        // JobDAOImpl.agenda.now(name, data);
      } else {
        JobDAOImpl.agenda.schedule(date, name, data);
      }
      JobDAOImpl.agenda.start();
      resolve(true);
    });
  }

  //"nextRunAt" attribute not null only in one trigger(i.e. The next executing trigger)
  getScheduleJobInfo(scheduleId: string): Promise<object[]> {
    return new Promise((resolve, reject) => {
      // const agenda = this.agendaConnection.connect();
      // agenda.on("ready", function () {
        let query = {
          "data.wfScheduleData.scheduleId":scheduleId,
          "nextRunAt":{$ne:null}
        };
        //todo : Fill the raw data to a model and resolve
        JobDAOImpl.agenda.jobs(query, (err, result) => {
          if (err) {
            winston.error("JobDAOImpl deleteJobs:" + err.stack);
            reject(err);
          }
          resolve(result);
        });
      });
    // });
  }

  deleteJobs(scheduleId: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      // const agenda = this.agendaConnection.connect();
      // agenda.on("ready", function () {
        var deleteQuery = {
          "data.wfScheduleData.scheduleId": scheduleId,
        };
        JobDAOImpl.agenda.cancel(deleteQuery, (err, result) => {
          if (err) {
            winston.error("JobDAOImpl deleteJobs:" + err.stack);
            reject(err);
            return;
          }
          resolve((result>0));
        });
      });
    // });
  }

  //
  // updateSchedule(configExecuteOnce: object): Promise<object> {
  //   return new Promise((resolve,reject)=>{
  //     this.deleteJobs("123").then((deleteSuccess)=>{
  //       if(deleteSuccess){
  //         const serviceFactory = new ServiceFactory();
  //         let scheduleId = "123";
  //         let wfRruleImpl:WfRrule<RRule.Options> = new WfRruleImpl();
  //         let rule = wfRruleImpl.generateRuleOptions(configExecuteOnce);
  //         let date:Date = wfRruleImpl.getExecutionDate(rule);
  //         const jobService = <JobService> serviceFactory.getService("JobService");
  //
  //         // this.scheduleJob(configExecuteOnce.).then((createSuccess)=>{
  //         //   if(createSuccess){
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //         //   }
  //         // });
  //       }
  //     })
  //   });
  // }


  /*
  * Important : This method will delete all the triggers already executed in mongodb
  * todo schedule executing this to clean db
  * */
  deleteExecutedJobs(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      const agenda = this.agendaConnection.connect();
      // agenda.on("ready", function () {

        let deleteQuery = {"nextRunAt":null};

        JobDAOImpl.agenda.cancel(deleteQuery, (err, result) => {
          if (err) {
            winston.error("JobDAOImpl deleteExecutedJobs:" + err.stack);
            reject(err);
            return;
          }
          resolve(true);
        });
      });
    // });
  }



  disableJob(scheduleId: string): Promise<boolean>  {
    return new Promise((resolve, reject) => {
      // agenda.on("ready", function () {
        var disableJob = {"data.wfScheduleData.scheduleId":scheduleId,"nextRunAt":{$ne:null}}
        JobDAOImpl.agenda.jobs(disableJob, (err, result) => {
          if (err) {
            winston.error("JobDAOImpl disableJob:" + err.stack);
            reject(err);
            return;
          }
          result[0].disable().schedule();
          result[0].save(function(err) {
            if(!err) {
              resolve(true);
            }
          })

        });
      });
    // });
  }

  enableJob(scheduleId: string):Promise<boolean> {
    return new Promise((resolve, reject) => {
      const agenda = this.agendaConnection.connect();
      // agenda.on("ready", function () {
        var enableJob = {"data.wfScheduleData.scheduleId":scheduleId,"nextRunAt":{$ne:null}}
        JobDAOImpl.agenda.jobs(enableJob, (err, result) => {
          if (err) {
            winston.error("JobDAOImpl enable:" + err.stack);
            reject(err);
            return;
          }
          result[0].enable()
          result[0].save(function(err) {
            if(!err) {
              resolve(true);
            }
          })
        });
      });
    // });
  }

  resumeAllPendingTriggers(): void {
    const agenda = this.agendaConnection.connect();
    JobDAOImpl.agenda.on("ready", () => {
      JobDAOImpl.agenda.start();
    });
  }
}
