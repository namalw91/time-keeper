export interface JobDAO{
  registerJob(name: string, taskTrigger: any,reScheduleCb: any): void;
  scheduleJob(date: Date, name: string, data: any): Promise<boolean>;

  //todo : change object array into a "JobModel" model list
  getScheduleJobInfo(scheduleId: string):Promise<object[]>
  // updateSchedule(data: object): Promise<object>;
  deleteJobs(scheduleId: string): Promise<boolean>;
  disableJob(scheduleId:string): Promise<boolean> ;
  enableJob(scheduleId:string): Promise<boolean> ;

  resumeAllPendingTriggers(): void;

}
