import * as express from "express";
import * as config from "config";
import * as winston from "winston";
const app = express();
import {ExpressConfig} from "./config/ExpressConfig";
new ExpressConfig(app);

const serverPort = process.env.PORT || config.get("server.port");

import {ConnectionFactory} from "./connections/factory/ConnectionFactory";
import {DatabaseConnection} from "./connections/DatabaseConnection";
import {ServiceFactory} from "./services/factory/ServiceFactory";
import {JobService} from "./services/JobService";
import {HttpsUrlConnectionImpl} from "./connections/impl/URLConnectionImpl";
import {URLConnection} from "./connections/URLConnection";
import {MessageConnection} from "./connections/MessageConnection";
import Agenda = require("agenda");

const connectionFactory = new ConnectionFactory();
const mongooseConnection = <DatabaseConnection> connectionFactory.getConnection("MongoDatabaseConnection");


app.listen(serverPort, () => {
  console.log("Express server listening on port " + serverPort);
});

const mongoose = mongooseConnection.connect();


var connectToMongoWithRetry = function () {
  mongoose.connect(config.get("database.url").toString(), {
    server: {
      auto_reconnect: true,
      reconnectTries: Number.MAX_VALUE,
      numberOfRetries: Number.MAX_VALUE
    }
  })
    .then(() => {
      var db = mongoose.connection;
      console.info("Connected to the database ");
      applicationDidStart();
      db.on('open', () => {
        console.info("Connected to the database");
      });
      db.on("error", (err) => {
        console.error("Database error: " + err);
      })
      db.on("disconnected", () => {
        console.warn("disconnected from mongo")
      })
      db.on("reconnected", () => {
        console.info("reconnected to mongo")
      })
    })
    .catch((err) => {
      if (err.message && err.message.match(/failed to connect to server .* on first connect/)) {
        console.error('Failed to connect to mongo on startup - retrying in 5 sec', err);
        setTimeout(connectToMongoWithRetry, 5000);
      } else {
        console.error('Failed to connect to mongo on startup', err);
      }
    });
  return;
};
connectToMongoWithRetry();



function applicationDidStart() {
  registerAllJobs();
  resumeAllJobs();
  console.log("Timekeeper started");
}



////////////////////////////////////////////////


const serviceFactory = new ServiceFactory();
const jobService = <JobService> serviceFactory.getService("JobService");

function registerAllJobs() {
  jobService.registerAllJob()
  winston.info("Registered all jobs");
}

function resumeAllJobs(){
  jobService.resumeAllPendingTriggers();
  console.log("Resume all triggers")
}
