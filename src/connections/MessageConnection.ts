import {Connection} from "./Connection";
export interface MessageConnection extends Connection {

  send(topic: string, message: any): void;
  receive(consumers: any): void;

}
