import {Connection} from "./Connection";
import Agenda = require("agenda");

export interface AgendaConnection extends Connection{
  connect():Agenda;
}
