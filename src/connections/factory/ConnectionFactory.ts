import {Connection} from "../Connection";
import {MongoDatabaseConnectionImpl} from "../impl/MongoDatabaseConnectionImpl";
import {DatabaseConnection} from "../DatabaseConnection";
import {URLConnection} from "../URLConnection";
import {HttpsUrlConnectionImpl} from "../impl/URLConnectionImpl";
import {MessageConnection} from "../MessageConnection";
import {KafkaConnectionImpl} from "../impl/KafkaConnectionImpl";

export class ConnectionFactory {
  private connection: Connection = null;
  private messageConnection: MessageConnection = new KafkaConnectionImpl();
  private databaseConnection: DatabaseConnection = new MongoDatabaseConnectionImpl();
  private urlConnectionImpl: URLConnection = new HttpsUrlConnectionImpl();

  public getConnection(c: string): Connection {

    switch (c) {
      case "MongoDatabaseConnection":
        this.connection = this.databaseConnection;
        break;

      case "HttpsUrlConnection":
        this.connection = this.urlConnectionImpl;
        break;

      case "KafkaConnection":
        this.connection = this.messageConnection;
        break;

      default:
        throw new Error("unsupported connection type");
    }

    return this.connection;

  }
}
