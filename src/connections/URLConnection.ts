import {Connection} from "./Connection";

export interface URLConnection extends Connection{
  sendRequest(data: any, options: any): any;
}
