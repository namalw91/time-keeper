import {MessageConnection} from "../MessageConnection";
const KafkaProducer = require("@workflows/np-producer");
const KafkaConsumer = require("@workflows/np-consumer");

export class KafkaConnectionImpl implements MessageConnection {

  private _options: {};
  private _kafkaConsumer: any;
  private _kafkaProducer: any;

  constructor() {
    this._options = {
      prefix: process.env.NODE_ENV || "localhost",
      // prefix: "localhost",
      cluster: "ENGINE"
    };
  }

  connect(): any {
    if (!this._kafkaProducer) {
      this._kafkaProducer = new KafkaProducer(this._options);
    }
    if (!this._kafkaConsumer) {
      this._kafkaConsumer = new KafkaConsumer(this._options);
    }
  }

  send(topic: string, message: any): void {
    this._kafkaProducer.produce(topic, JSON.stringify(message));
  }

  receive(consumers: any): void {
    consumers.forEach((consumer: any) => {
      this._kafkaConsumer.subscribe([consumer.topic], consumer.cb);
    });
  }

  get kafkaConsumer(): any {
    return this._kafkaConsumer;
  }

  get kafkaProducer(): any {
    return this._kafkaProducer;
  }

}
