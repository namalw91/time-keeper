import {DatabaseConnection} from "../DatabaseConnection";
import * as mongooseInstance from "mongoose";

export class MongoDatabaseConnectionImpl implements DatabaseConnection {

  constructor() {

  }

  connect(): any {
    return mongooseInstance;
  }
}
