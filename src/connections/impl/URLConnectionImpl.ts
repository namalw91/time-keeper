import {URLConnection} from "../URLConnection";
import * as https from "http";
import * as winston from "winston";


export class HttpsUrlConnectionImpl implements URLConnection {

  constructor() {

  }

  connect(): string {
    return "https url connection instance";
  }

  sendRequest(data: any, options: any): any {
    return new Promise((resolve, reject) => {

      const req = https.request(options, (res) => {
        let result = "";
        res.on("data", (chunk: any) => {
          result += chunk;
        });
        res.on("end", () => {
          resolve(result);
        });
        res.on("error", (err: any) => {
          reject(err);
        });
      });

      req.on("error", (err: any) => {
        winston.error("httpeee error " + err);
        reject(err);
      });

      req.write(data);
      req.end();

    }).catch((err) => {
      winston.error("http connection error ->> " + err);
    });
  }

}
