import {AgendaConnection} from "../AgendaConnection";
import Agenda = require("agenda");
import * as Config from "config";

export class AgendaConnectionImpl implements AgendaConnection{
  private _agenda : Agenda;

  constructor(){

  }

  connect(): Agenda {
    if (!this._agenda) {
      this._agenda = new Agenda({
        db: {address: Config.get<string>("database.url"), collection: "agendaJobs"},
        maxConcurrency: 20,
        defaultConcurrency: 5
      });

    }
    return this._agenda;
  }

}
