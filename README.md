# Workflows - Bot SDK

This SDK helps developers to build and test automation bots for Workflows

## Usage

Install **workflows-bot-sdk**.

```sh
$ npm install workflows-bot-sdk --save-dev
```

_TODO: Usage details_

### [Changelog](CHANGELOG.md)

---

### SDK Development

**Install Packages**

```sh
$ npm install
```

**Available NPM Commands**
```sh
Usage:
  npm run <task>
  
Available tasks:
  clean        Cleans the generated js files from `/lib` directory
  build        Compiles all TypeScript source files [clean, lint]
  lint         Lints all TypeScript source files
  test         Runs the Mocha test specs [build]
  watch        Watches ts source files and runs `build` on change
  watch:test   Watches ts source files and runs `test` on change
```

## License

Apache-2.0
